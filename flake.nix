{
  description = "ZMK config Nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    mach-nix.url = "github:DavHau/mach-nix";
  };

  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux" "aarch64-darwin"];

      perSystem = {
        pkgs,
        system,
        ...
      }: let
        westManifest = builtins.fromJSON (builtins.readFile ./west-manifest.json);
        getWestModule = args @ {
          name,
          url,
          sha256,
          revision,
          clone-depth ? null,
          ...
        }: {
          path = args.path or name;
          root = !args ? path;
          src = pkgs.fetchgit {
            inherit url sha256;
            rev = revision;
          };
        };
        zephyr = getWestModule (builtins.head (builtins.filter ({name, ...}: name == "zephyr") westManifest));
        pythonPkgs = inputs.mach-nix.lib."${system}".mkPython {
          requirements = builtins.readFile "${zephyr.src}/scripts/requirements-base.txt";
        };
        buildTools = [pkgs.cmake pkgs.ccache pkgs.ninja pkgs.dtc pkgs.dfu-util pkgs.gcc-arm-embedded pythonPkgs];
        exportToolchain = ''
          # Export Zephyr toolchain
          export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
          export GNUARMEMB_TOOLCHAIN_PATH=${pkgs.gcc-arm-embedded}
        '';
      in {
        apps.updateWestManifest = {
          type = "app";
          program =
            (pkgs.writeScript
              "update-west-manifest"
              ''
                repodir="$PWD"
                export PATH=${pkgs.lib.makeBinPath [pythonPkgs pkgs.remarshal pkgs.nix-prefetch-git pkgs.jq pkgs.git]}:$PATH
                tempdir=$(mktemp -d)

                pushd $tempdir
                cp -R ${./config} ./config
                west init -l config
                west update
                ${./update-west-manifest.sh} > "$repodir/west-manifest.json"
                popd

                chmod -R +w $tempdir
                rm -rf $tempdir
              '')
            .outPath;
        };

        devShells.default = pkgs.mkShell {
          nativeBuildInputs = [] ++ buildTools;
          shellHook = ''${exportToolchain}'';
        };

        legacyPackages.zmkBinary = {
          config,
          board,
          shield ? null,
        }:
          pkgs.stdenv.mkDerivation {
            name = "zmkBinary";
            nativeBuildInputs = [pkgs.git] ++ buildTools;
            dontUseCmakeConfigure = true;
            dontUnpack = true;
            buildPhase = let
              westModules = map getWestModule westManifest;
              installWestModule = module:
                if module.root
                then ''
                  # Generate a fake git repository for the module to be recognized by Zephir
                  mkdir -p ${module.path}
                  cp -r ${module.src}/. ${module.path}
                  cd ${module.path}
                  git init
                  git config user.email @
                  git config user.name @
                  git add .
                  git commit -m init
                  git update-ref refs/heads/manifest-rev HEAD
                  cd ../
                ''
                else ''
                  mkdir -p $(dirname ${module.path})
                  ln -s ${module.src} ${module.path}
                '';
            in ''
              ${exportToolchain}

              # Export Zephyr Core (west zephyr-export)
              export CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:$PWD/zephyr/share/zephyr-package/cmake
              export CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:$PWD/zephyr/share/zephyrunittest-package/cmake

              # Export a home directory for build artifacts
              export HOME=$PWD/home
              mkdir home

              # Copy config files
              mkdir config
              cp ${./config/west.yml} config/west.yml
              cp -r ${config}/. config

              # Install West modules
              ${builtins.concatStringsSep "\n" (map installWestModule westModules)}

              # Build the ZMK binary
              west init -l config
              west build -s zmk/app -b ${board} -- -DZMK_CONFIG=$PWD/config ${
                if shield != null
                then "-DSHIELD=${shield}"
                else ""
              }
            '';
            installPhase = ''
              mkdir -p $out/bin
              mv build/zephyr/zmk.uf2 $out/bin
            '';
          };
      };
    };
}
