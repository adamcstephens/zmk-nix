# Archived

Switched to https://github.com/lilyinstarlight/zmk-nix

# Build configured ZMK binaries using Nix

Build ZMK binaries locally using Nix instead of GitHub Actions.

Example usage: https://github.com/adamcstephens/zmk-config

## Updating the West manifest

```bash
# run the following from the repo root
nix run .#updateWestManifest
```
