update-west:
    rm -rf .west
    west init -l config
    west update
    WEST_MANIFEST=$(nix run .#updateWestManifest) && echo "$WEST_MANIFEST" >west-manifest.json
